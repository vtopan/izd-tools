"""
Associates the .izd file extension with vizd.pyw on Windows.

Author: Vlad Topan
"""
import winreg
import os
import sys


TITLE = 'IZD_Document'
PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def die(*messages):
    for msg in messages:
        if msg:
            sys.stderr.write(msg + '\n')
    input('[FAILED] Press enter to continue...')
    sys.exit()


def set_key_def_value(root, keyname, value):
    key = winreg.CreateKey(root, keyname)
    if exists := winreg.QueryValue(key, None):
        winreg.DeleteKey(winreg.HKEY_LOCAL_MACHINE, keyname)
        winreg.CloseKey(key)
        key = winreg.CreateKey(root, keyname)
    winreg.SetValue(key, None, winreg.REG_SZ, value)


print('[*] Retrieving path to pythonw.exe...')
try:
    key = winreg.OpenKey(winreg.HKEY_CLASSES_ROOT, r'Python.NoConFile\Shell\open\command')
except FileNotFoundError:
    die(f'[!] Failed retrieving pythonw.exe path - is Python installed?')
pyw_path = winreg.QueryValue(key, None)
pyw_path = pyw_path.split('"')[1]
print(f'[*] pythonw.exe path: {pyw_path}')
HKLM = winreg.HKEY_LOCAL_MACHINE
print('[*] Setting up the .izd file extension...')
#set_key_def_value(HKLM, 'Software\\Classes\\.izd', TITLE)
#set_key_def_value(HKLM, f'Software\\Classes\\{TITLE}\\Shell\\open\\command', f'"{pyw_path}" "{PATH}\\vizd\\vizd.pyw" "%L"')
os.system(f'assoc .izd={TITLE}')
os.system(f'ftype {TITLE}="{pyw_path}" "{PATH}\\vizd\\vizd.pyw" "%L"')
set_key_def_value(HKLM, f'Software\\Classes\\{TITLE}\\DefaultIcon', f'{PATH}\\vizd\\vizd.ico,0')
print('[*] Done.')
